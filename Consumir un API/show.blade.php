<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="stylesheet" type="text/css"  href="/css/app.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Api</title>
</head>
<body>
        <div class= "container">
                <h1> POSTS </h1>
                
                <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4>{{ $post->title }}</h4>

                                <div class="panel-body">
                                        {{ $post->body }}
                                        <br>
                                        <a href="/">Volver a la raíz</a>
                                </div> 
                        </div>
                </div>
        </div>
</body>
</html>