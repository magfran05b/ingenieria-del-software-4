<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
</head>
<body>
        <?php
           print 'Versión actual de PHP es: ' . phpversion();
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'El ID de la version PHP es : '.PHP_VERSION_ID;
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'El valor máximo soportado para enteros para esa versión es: '.PHP_INT_MAX;
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'Tamaño máximo del nombre de un archivo es: '.PHP_MAXPATHLEN;
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'Versión del Sistema Operativo es : ' .PHP_OS_FAMILY;
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'Símbolo correcto de Fin De Línea para la plataforma en uso es : '.PHP_EOL."+";
           print '<br>';
           print '------------------------------------------------------------------------------';
           print '<br>';
           print 'El include path por defecto es ' .DEFAULT_INCLUDE_PATH;


?>
        
</body>
</html>