-- Tabla productos
create table Productos (
idproducto serial primary key,
idmarca smallint,
idcategoria smallint, 
nombre varchar(100) not null,
precio decimal );
select * from productos;

-- Tabla categoria
create table Categoria (
idcategoria serial primary key,
nombre varchar(100) not null );
select * from categoria;

-- Tabla marca
create table Marca (
idmarca serial primary key,
idempresa smallint,
nombre varchar(100) not null );
select * from marca;

-- Tabla empresa
create table empresa (
idempresa serial primary key,
nombre varchar(100) not null );	
select * from empresa;

select pro.nombre as nombre_productos, pro.precio as precio_productos,
ma.nombre as nombre_marca, ne.nombre as nombre_empresa, nc.nombre as nombre_categoria
from productos as pro
inner join Marca as ma on pro.idmarca = ma.idmarca 
inner join empresa as ne on ma.idempresa = ne.idempresa
inner join Categoria as nc on pro.idcategoria = nc.idcategoria
	
	
