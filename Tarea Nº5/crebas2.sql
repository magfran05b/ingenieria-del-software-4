CREATE TABLE alumnos (
  id              SERIAL PRIMARY KEY,
  nombre          VARCHAR(50) NOT NULL,
  apellido        VARCHAR(50) NOT NULL,
  cedula          VARCHAR(50) NOT NULL,
  matricula       VARCHAR(50) NOT NULL,
  carrera         VARCHAR(50) NOT NULL,
  nacionalidad    VARCHAR(50) NOT NULL
);
SELECT * FROM alumnos