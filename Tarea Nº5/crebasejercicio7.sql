
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- DDL
CREATE TABLE USUARIOS (
   ID    SERIAL  NOT NULL,
   USUARIO VARCHAR(35) NOT NULL,
   PASSWORD VARCHAR(35) NOT NULL,
   CONSTRAINT PK_USUARIOS PRIMARY KEY (ID)
);
SELECT * FROM USUARIOS

INSERT INTO USUARIOS VALUES (1, 'world', crypt('world', gen_salt('md5')));
INSERT INTO USUARIOS VALUES (2, 'sky', crypt('sky', gen_salt('md5')));

-- Tabla PRODUCTO
create table PRODUCTO (
PRODUCTO_ID  SERIAL NOT NULL,
TIPO_ID   INT4 NOT NULL,
MARCA_ID  INT4 NOT NULL,
NOMBRE  VARCHAR(40) NOT NULL,
DESCRIPCION VARCHAR(40) NOT NULL,
CONSTRAINT PK_PRODUCTO PRIMARY KEY(PRODUCTO_ID)
);
-- Tabla MARCA
create table MARCA (
MARCA_ID  SERIAL NOT NULL,
NOMBRE    VARCHAR(40) NOT NULL,
CONSTRAINT PK_MARCA PRIMARY KEY (MARCA_ID)
);
-- Tabla TIPO
create table TIPO (
TIPO_ID  SERIAL NOT NULL,
NOMBRE   VARCHAR(40) NOT NULL,
CONSTRAINT PK_TIPO PRIMARY KEY (TIPO_ID)
);
SELECT * FROM PRODUCTO, TIPO, MARCA
insert into tipo values(1,'Bebidas Alcoholicas');
insert into tipo values(2,'Gaseosas');
insert into tipo values(3,'Carnes');
insert into tipo values(4,'Embutidos');
insert into tipo values(5,'Bebidas Nutritivas');
insert into tipo values(6,'Postres');
insert into tipo values(7,'Bebidas');
insert into tipo values(8,'Jugos');
ALTER SEQUENCE tipo_tipo_id_seq RESTART WITH 9;
SELECT * FROM TIPO
insert into marca values(1,'Miller');
insert into marca values(2,'Pepsi');
insert into marca values(3,'Coca Cola');
insert into marca values(4,'Fran');
insert into marca values(5,'Cervepar');
insert into marca values(6,'Heineken');
insert into marca values(7,'Brahma')
insert into marca values(8,'Pilsen')
insert into marca values(9,'Fanta Naranja')
ALTER SEQUENCE marca_marca_id_seq RESTART WITH 10;
SELECT * FROM MARCA
DELETE FROM MARCA
insert into producto values(1,2,1,'Miller 3/4','Mil....');
insert into producto values(3,2,7,'Miller Latita','Mil....');
insert into producto values(5,5,6,'Heineken 1L','Hei....');
insert into producto values(4,2,8,'Brahma 3/4','Bra....');
insert into producto values(8,3,1,'Pilsen 3/4','Bra....');
insert into producto values(2,3,9,'Naranja 500ml','Fanta Naranja');
insert into producto values(6,4,6,'Flan Chico','Fla..');
insert into producto values(7,3,6,'Powered','Descripcion');
insert into producto values(9,3,6,'Powered','');
ALTER SEQUENCE producto_producto_id_seq RESTART WITH 10;
SELECT * FROM producto
