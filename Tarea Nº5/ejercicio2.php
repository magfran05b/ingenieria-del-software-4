<?php
	require_once 'vendor/autoload.php';
	include_once ('conexion2.php');
        $pdo = CConexion::ConexionBD();
	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
	$reader->setReadDataOnly(TRUE);
	$spreadsheet   = $reader->load("registro.xlsx");
	$worksheet     = $spreadsheet->getActiveSheet();
	$highestRow    = $worksheet->getHighestRow(); 
	$highestColumn = $worksheet->getHighestColumn(); 
	$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 

	print '<table border="1">' . "\n";
	for ($row = 1; $row <= $highestRow; ++$row) {
		print '<tr>' . PHP_EOL;
		for ($col = 1; $col <= $highestColumnIndex; ++$col) {
			$value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
			print '<td>' . $value . '</td>' . PHP_EOL;
		}
		print '</tr>' . PHP_EOL;
	}
	print '</table>' . PHP_EOL;

	$sql = "";

	for ($row = 2; $row <= $highestRow; ++$row) {
		$sql.="insert into alumnos(nombre,apellido,cedula,matricula,carrera,nacionalidad) values(";
		for ($col = 1; $col <= $highestColumnIndex; ++$col) {
			if($col==6)
				$sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."')";
			else
				$sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."',";
		}
		$sql.=";";
	}

	$resultado= pg_query($pdo, $sql);

		if (!$resultado) {
			print "Error volver a consultar";
			exit;
		} else {
			print "Se ingreso los datos";
		}
?>