<?php
        require_once("ClassPersona.php");

        class Empleado extends Persona{
         //Atributos
        private $codigoEmpleado;

        //funcion constructor
        function __construct($nombre, $apellido, $cedula, $direccion, $edad, $codigoEmpleado){
        parent:: __construct($nombre, $apellido, $cedula, $direccion, $edad);
        $this->codigoEmpleado= $codigoEmpleado;
        }
        
        //metodos
        public function getcodigoEmpleado(){
                return $this->codigoEmpleado;
        }
       
        public function setcodigoEmpleado(int $codigoEmpleado){
                if($codigoEmpleado<0){
                        print "Debe ser mayor a 0";
                        exit;
                }
         $this->codigoEmpleado= $codigoEmpleado;
        }
        
        

}


?>