<?php
   class Persona {
        //Atributos
       private $nombre;
       private $apellido;
       private $cedula;
       private $direccion;
       private $edad;
       private $telefono = array();

       //funcion constructor
       function __construct($nombre, $apellido, $cedula, $direccion, $edad){
               $this->nombre = $nombre;
               $this->apellido = $apellido;
               $this->cedula = $cedula;
               $this->direccion = $direccion;
               $this->edad = $edad;
       }
    
       //metodos GET
        public function getnombre(){
         return $this->nombre;
        }
        
        public function getapellido(){
                return $this->apellido;
        }

        public function getcedula(){
                return $this->cedula;
        }

        public function getdireccion(){
                return $this->direccion;
        }

        public function getedad(){
                return $this->edad;
        }

        //metodos SET
        public function setnombre(string $nombre){
                $this->nombre= $nombre;
        }
        
        public function setapellido(string $apellido){
                $this->apellido= $apellido;
        }
        
        public function setcedula(string $cedula){
                $this->cedula= $cedula;
        }

        public function setdireccion(string $direccion){
                $this->direccion= $direccion;
        }

        public function setedad(int $edad){
                if($edad<0){
                print "Debe ser mayor a 0";
                exit;
                }
        $this->edad= $edad;
        }
        public function setTelefono(string $tipo, string $numero){
                $this->telefono[] = new telefono($tipo, $numero);
        }
}
?>